<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'CastController@home');
Route::get('/add', function(){
	return view('add');
});

Route::POST('/insert', 'CastController@add');
Route::get('/update/{id}', 'CastController@update');
Route::get('/edit/{id}', 'CastController@edit');
Route::get('/read/{id}', 'CastController@read');
Route::get('/delete/{id}', 'CastController@delete');