<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function home(){

    	$casts = Cast::all();
    	return view('home', ['casts' => $casts]);
    }

    public function add(Request $request){
    	$this->validate($request, [
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required']);
    	$casts = new Cast;
    	$casts->nama = $request->input('nama');
    	$casts->umur = $request->input('umur');
    	$casts->bio = $request->input('bio');
    	$casts->save();
    	return redirect('/')->with('info', 'Cast saved successfully!');
    }

    public function update($id){
    	$casts = Cast::find($id);
    	return view('update', ['cast' => $casts]);
    }

    public function edit(Request $request,$id){
    	$this->validate($request, [
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required']);
    	$data = array(
    		'nama' => $request ->input('nama'),
    		'umur' => $request ->input('umur'),
    		'bio' => $request ->input('bio')
    		);
    	Cast::where('id', $id)->update($data);
    	return redirect('/')->with('info', 'update successfully!');
    
    }

    public function read($id){
    	$casts = Cast::find($id);
    	return view('read', ['cast' => $casts]);
    }

    public function delete($id){
    	Cast::where('id', $id)->delete();
    	return redirect('/')->with('info', 'Delete successfully');
    }
}
