@include('inc.header')
  
      
      <form method="get" action="{{ url('/edit',array($cast->id)) }}">
      {{csrf_field()}}
        <div class="modal-header">
          <h4 class="modal-title">Add Employee</h4>
          
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" value="<?php echo $cast->nama; ?>">
          </div>
          
          <div class="form-group">
            <label>Umur</label>
            <input type="text" name="umur" class="form-control" value="<?php echo $cast->umur; ?>">
          </div>

          <div class="form-group">
            <label>Bio</label>
            <input type="text" name="bio" class="form-control" value="<?php echo $cast->bio; ?>">
          </div>
         
        </div>
        <div class="modal-footer">
          <a href="{{ url('/') }}" type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">Back</a>
         
          <input type="submit" class="btn btn-success" value="submit">
        </div>
      </form>
    

@include('inc.footer')